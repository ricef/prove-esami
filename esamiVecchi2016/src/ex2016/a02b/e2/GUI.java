package ex2016.a02b.e2;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.swing.*;

public class GUI extends JFrame {

	private static final long serialVersionUID = 1L;
	private final List<JButton> buttons;
    private final Model model;
    
    
	public GUI(int size){
		final Agent agent = new Agent();
		final JPanel jp = new JPanel();
		final JFrame jf = new JFrame();
		model = new ModelImpl(size);
		buttons = new ArrayList<>();
		for(int i=0;i<size;i++) {
			buttons.add(new JButton("  "));
			jp.add(buttons.get(i));
		}
		
        jf.getContentPane().add(jp);
        jf.pack();
        jf.setVisible(true);		
        agent.start();
	}
	
	private class Agent extends Thread {

		private volatile boolean stop = false;
		private int counter; // the mode here is just a counter!
		
		public void run() {
			while (!stop) {
			    counter++;
			    String s = ""+counter; // creating a local copy
			    SwingUtilities.invokeLater(()->{
			    	refresh();
			    });
				try {
					Thread.sleep(300);
				} catch (Exception ex) {
					System.out.println(ex);
					ex.printStackTrace();
				}
			}
		}

		public void stopAdvancing() {
			this.stop = true;
		}
	}
	
	private void refresh () {
		buttons.forEach(b -> {
			b.setText(" ");
			buttons.get(model.positions().get(buttons.indexOf(b))).setText(" * ");
		});
	}
	public static void main(String[] s){
	    new GUI(10);
	}
}
