package ex2016.a02b.e2;

import java.util.ArrayList;
import java.util.List;

public class ModelImpl implements Model {
	
	private final int size;
	private List<Integer> positions;
	
	public ModelImpl(int size) {
		this.size = size;
		reset();
	}
	
	public void advance() {
		positions.forEach(el -> el = (el+1)%size);
	}
	
	public List<Integer> positions(){
		return positions;
	}
	
	public void reset() {
		positions = new ArrayList<>();
		for(int i=0; i<size; i++) {
			positions.add(i);
		}
	}

}
