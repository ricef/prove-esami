package ex2016.a02b.e2;

import java.util.List;

public interface Model {
	
	public void advance();
	
	public List<Integer> positions();
	
	public void reset() ;
}
