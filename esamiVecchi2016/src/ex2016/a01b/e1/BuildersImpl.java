package ex2016.a01b.e1;

import java.util.Collection;
import java.util.Collections;


public class BuildersImpl implements Builders {

	@Override
	public <X> ListBuilder<X> makeBasicBuilder() {
		return new ListBuilderImpl<>(Collections.emptyList(), -1);
	}

	@Override
	public <X> ListBuilder<X> makeBuilderWithSize(int size) {
		return new ListBuilderImpl<X>(Collections.emptyList(), size);
	}

	@Override
	public <X> ListBuilder<X> makeBuilderFromElements(Collection<X> from) {
		return new ListBuilderImpl<X>(from, -1);
	}

	@Override
	public <X> ListBuilder<X> makeBuilderFromElementsAndWithSize(Collection<X> from, int size) {		
		return new ListBuilderImpl<X>(from, size);
	}

}
