package ex2016.a01b.e1;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class ListBuilderImpl<X> implements ListBuilder<X> {

	private final List<X> list = new ArrayList<>();
	private final Collection<X> containable;
	private final int size;			
	private boolean done = false;
	
	public ListBuilderImpl(Collection<X> from, int size) {
		containable = from;
		this.size = size;
	}
	
	@Override
	public void addElement(X x) {
		if (done == false) {
			if ((size==-1 || size > list.size() ) && 
					(containable.contains(x) || containable.isEmpty())) {
				this.list.add(x);
			} else {
				throw new IllegalArgumentException();
			}
		}else {
			throw new IllegalArgumentException();
		}
	}

	@Override
	public List<X> build() {
		if (done == false) {
			if (size == -1 || list.size() == size) {
				done = true;
				return Collections.unmodifiableList(list);
			}
		} 
		throw new IllegalStateException();
	}
}
