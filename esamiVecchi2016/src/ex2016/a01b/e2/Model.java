package ex2016.a01b.e2;

public interface Model {
	
	int next();
	boolean hasNext();
}
