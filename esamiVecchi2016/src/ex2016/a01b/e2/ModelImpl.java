package ex2016.a01b.e2;

import java.io.*;
import java.util.*;

public class ModelImpl implements Model {
	
	private final Iterator<Integer> iterator;

	public ModelImpl(final String fileName) {
		final List<Integer> list = new ArrayList<Integer>();
		
		try (final BufferedReader reader= new BufferedReader(new FileReader (fileName))) {
			String s;
			while ((s= reader.readLine()) != null){
				list.add(Integer.parseInt(s));
			}
		} catch (FileNotFoundException e) {
			System.out.println("The file is not found.");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("IOException catched.");
			e.printStackTrace();
		}
		
		iterator = list.iterator();
	}

	public int next() {
		return iterator.next();
	}
	
	public boolean hasNext() {
		return iterator.hasNext();
	}
}
