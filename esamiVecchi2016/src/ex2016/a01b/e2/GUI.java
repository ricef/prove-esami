package ex2016.a01b.e2;

import javax.swing.*;

public class GUI {
	
	private Model mod;
    private JButton[] buttons;
    
    public GUI(String fileName, int size){
    	this.mod = new ModelImpl(fileName);
    	this.buttons = new JButton[size];
    	
    	final JFrame jf = new JFrame();
    	final JPanel jp = new JPanel();
    	
    	
    	for(int i=0; i<size;i++) {
    		buttons[i] = new JButton(String.valueOf(i));
    		buttons[i].addActionListener(e -> setButtons());
    		jp.add(buttons[i]);
    	}
    	setButtons();
    	
    	final JButton reset = new JButton("Reset");
        reset.addActionListener(e -> {
        	mod = new ModelImpl(fileName);
        	setButtons();
        });
        jp.add(reset);
        
        jf.getContentPane().add(jp);
        jf.pack();
        jf.setVisible(true);
    }

    private void setButtons() {
    	int index=0;
    	int enabled;
    	if (mod.hasNext()) {
    		enabled=mod.next();
    	} else {
    		enabled=-1;
    	}
    	
    	for (JButton b: buttons) {
    		b.setEnabled(enabled == index);
    		index++;
    	}
    }
}
