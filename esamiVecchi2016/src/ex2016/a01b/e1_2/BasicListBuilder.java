package ex2016.a01b.e1_2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BasicListBuilder<X> implements ListBuilder<X>{

	private final List<X> list = new ArrayList<>();
	private boolean builded = false;
	
	public BasicListBuilder() {
	}
	
	@Override
	public void addElement(X x) {
		if (builded == false) {
			this.list.add(x);
		} else {
			throw new IllegalStateException();
		}
	}

	@Override
	public List<X> build() {
		if (builded == false) {
			builded=true;
			return Collections.unmodifiableList(list);
		}
		throw new IllegalStateException();
	}
	
}
