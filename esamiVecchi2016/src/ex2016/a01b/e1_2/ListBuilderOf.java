package ex2016.a01b.e1_2;

import java.util.Collection;
import java.util.List;

public class ListBuilderOf<X> implements ListBuilder<X>{

	private final ListBuilder<X> builder;
	private final Collection<X> containable ;
	
	public ListBuilderOf(ListBuilder<X> builder, Collection<X> from) {
		containable = from;
		this.builder = builder;
	}
	@Override
	public void addElement(X x) {
		if (containable.contains(x)) {
			builder.addElement(x);
		} else {
			throw new IllegalArgumentException();
		}
	}

	@Override
	public List<X> build() {
		return builder.build();
	}

}
