package ex2016.a03b.e1;

public class ClockImpl implements Clock{

	private Time time;
	
	public ClockImpl(final Time time) {
		this.time = time;
	}
	@Override
	public Time getTime() {
		return time;
	}

	@Override
	public void tick() {
		int hour = time.getHours();
		int minutes = time.getMinutes();
		int seconds = time.getSeconds();
		if (seconds == 59) {
			seconds=0;
			if (minutes == 59) {
				minutes=0;
				if (hour == 23) {
					hour = 0;
				} else {
					hour ++;
				}
			} else {
				minutes ++;
			}
		} else {
			seconds++;
		}
		time = new TimeImpl(hour,minutes,seconds);
	}

	@Override
	public void registerAlarmObserver(Time time, Runnable observer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void registerHoursDeadlineObserver(int hours, Runnable observer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void registerMinutesDeadlineObserver(int minutes, Runnable observer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void registerSecondsDeadlineObserver(int seconds, Runnable observer) {
		// TODO Auto-generated method stub
		
	}

}
