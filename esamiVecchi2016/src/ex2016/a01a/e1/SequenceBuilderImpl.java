package ex2016.a01a.e1;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class SequenceBuilderImpl<X> implements SequenceBuilder<X>{

	private final List<X> list = new LinkedList<>();
	private boolean done = false;
	
	public SequenceBuilderImpl() {
	}
	
	@Override
	public void addElement(X x) {
		list.add(x);
	}

	@Override
	public void removeElement(int position) {
		list.remove(position);
	}

	@Override
	public void reverse() {
		Collections.reverse(list);
	}

	@Override
	public void clear() {
		list.clear();
	}

	@Override
	public Optional<Sequence<X>> build() {
		if (done == false) {
			done = true;
			return Optional.ofNullable(new SequenceImpl<>(list));
		} else {
			return Optional.empty();
		}
	}

	@Override
	public Optional<Sequence<X>> buildWithFilter(Filter<X> filter) {
		if (done == true || ! this.list.stream().allMatch(e -> filter.check(e))) {
			return Optional.empty();
		} else {
			done = true;
			return Optional.ofNullable(new SequenceImpl<>(list));
		}
	}

	@Override
	public <Y> SequenceBuilder<Y> mapToNewBuilder(Mapper<X, Y> mapper) {
		final SequenceBuilder<Y> builder = new SequenceBuilderImpl<>();
		list.forEach(e -> builder.addElement(mapper.transform(e)));
		return builder;
	}

}
