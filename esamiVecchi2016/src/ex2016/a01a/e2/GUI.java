package ex2016.a01a.e2;

import javax.swing.*;

public class GUI {
    
	
	
    public GUI(String fileName){
    	final Model mod = new ModelImpl (fileName);
        final JFrame jf = new JFrame();
        final JPanel jp = new JPanel();
        final JButton rand = new JButton("Rand");
        final JButton dec = new JButton("Dec");
        final JButton zero = new JButton("Zero");
        final JButton ok = new JButton("OK");
        
        rand.addActionListener(e -> {
        	mod.insertRandom();
        });
        
        dec.addActionListener(e -> {
        	mod.insertDecrement();
        });
        
        zero.addActionListener(e -> {
        	mod.insertZero();
        });
        
        ok.addActionListener(e->{
        	mod.writeOnFile();
        });
        
        
        jp.add(rand);
        jp.add(dec);
        jp.add(zero);
        jp.add(ok);
        
        jf.getContentPane().add(jp);
        jf.pack();
        jf.setVisible(true);
    }

}
