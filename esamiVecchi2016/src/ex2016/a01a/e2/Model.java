package ex2016.a01a.e2;

public interface Model {

	void insertRandom();
	void insertDecrement();
	void insertZero();
	void writeOnFile();
}
