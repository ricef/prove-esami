package ex2016.a01a.e2;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ModelImpl implements Model {

	private final Random seed = new Random();
	private int decremental = 0;
	private final File file;
	private final List<Integer> list = new ArrayList<>();
	
	public ModelImpl(String fileName) {
		file = new File(fileName);
	}
	
	@Override
	public void insertRandom() {
		this.list.add(seed.nextInt(9)+1);
	}

	@Override
	public void insertDecrement() {
		this.list.add(this.decremental--);

	}

	@Override
	public void insertZero() {
		this.list.add(0);
	}

	@Override
	public void writeOnFile() {
		try (final BufferedWriter writer = new BufferedWriter(new FileWriter(file))){
			for (int elem : list) {
				System.out.println(elem + " ");
				writer.write(String.valueOf(elem) + " ");
			}
		} catch (IOException e) {
			System.out.println("IOException generated.");
		}
	
	}
}