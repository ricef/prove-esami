package ex2016.a02a.e2;

import javax.swing.*;

import java.util.*;

public class GUI {
    
    private final List<JButton> buttons;
    private Model model;
    
    public GUI(int size){
    	model = new ModelImpl(size);
    	buttons = new ArrayList<>();
    	final JPanel jp = new JPanel();
    	final JFrame jf = new JFrame();
    	
    	for (int i=0;i<size;i++) {
    		final int index = i;
    		buttons.add(new JButton("  "));
    		buttons.get(i).addActionListener(e -> {
    			model.actionPerformed(index);
    		});
    		jp.add(buttons.get(i));
    	}
    	
    	final JButton move = new JButton("Move");
    	final JButton reset = new JButton("Reset");
    	
    	move.addActionListener(e -> {
    		model.move();
    		refreshDisplay();
    	});
        
    	reset.addActionListener(e -> {
    		model = new ModelImpl(size);
    		refreshDisplay();
    	});
    	
        jp.add(move);
        jp.add(reset);
        refreshDisplay(); /*Inizializzo*/
        jf.getContentPane().add(jp);
        jf.pack();
        jf.setVisible(true);
    }
    
    private void refreshDisplay() {
    	for (JButton b: buttons) {
    		b.setText("  ");
    	}
    	buttons.get(model.actualPosition()).setText(" * ");
    }
    
    public static void main(String[] s){
        new GUI(10);
    }

}
