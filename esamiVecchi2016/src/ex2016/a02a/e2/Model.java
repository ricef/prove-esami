package ex2016.a02a.e2;

public interface Model {
	
	enum Direction{
		DESTRA, SINISTRA;
	}

	void move();
	void actionPerformed(int index);
	int actualPosition();
}
