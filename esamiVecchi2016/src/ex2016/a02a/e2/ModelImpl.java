package ex2016.a02a.e2;

public class ModelImpl implements Model {

	private Direction direzione;
	private final boolean[] array;
	private int position;
	
	public ModelImpl(final int size) {
		direzione = Direction.DESTRA;
		array = new boolean[size];
		position = 0;
	}
	@Override
	public void move() {
		if (direzione == Direction.DESTRA) {
			if ( (position+1) == array.length || array[position+1]) { /*Ho incontrato una barriera*/ 
				if ((position-1) > 0 && ! array[position-1]) {
					direzione = Direction.SINISTRA;
					position--;					
				}
			} else {
				position++;
			}
		} else {
			if ((position-1) < 0 || array[position-1]) {
				if ((position+1) != array.length && ! array[position+1]) {
					direzione = Direction.DESTRA;
					position++;
				}
			} else {
				position--;
			}
		}
	}

	@Override
	public void actionPerformed(final int index) {
		array[index] = true;
	}

	@Override
	public int actualPosition() {
		return position;
	}

}
