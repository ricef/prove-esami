package ex2016.a02a.e1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

public class MultiQueueImpl<T,Q> implements MultiQueue<T, Q> {

	private final Map<Q,List<T>> queues;
	
	public MultiQueueImpl() {
		queues = new HashMap<>();
	}
	
	@Override
	public Set<Q> availableQueues() {
		return queues.keySet();
	}

	@Override
	public void openNewQueue(Q queue) {
		queues.put(queue , new ArrayList<>());
		
	}

	@Override
	public boolean isQueueEmpty(Q queue) {
		return queues.get(queue).isEmpty();
	}

	@Override
	public void enqueue(T elem, Q queue) {
		queues.get(queue).add(elem);	
	}

	@Override
	public Optional<T> dequeue(Q queue) {
		if (queues.get(queue).isEmpty()) {
			return Optional.empty();
		} else {		
			return Optional.of(queues.get(queue).remove(0));
		}
	}

	@Override
	public Map<Q, Optional<T>> dequeueOneFromAllQueues() {
		final Map<Q,Optional<T>> ret = new HashMap<>();
		queues.keySet().forEach( e -> ret.put(e, dequeue(e)));
		return ret;
	}

	@Override
	public Set<T> allEnqueuedElements() {
		final Set<T> ret = new HashSet<>();
		for (List<T> list : queues.values()) {
			ret.addAll(list);
		}
		return ret; 
	}

	@Override
	public List<T> dequeueAllFromQueue(Q queue) {
		final List<T> ret = queues.get(queue);
		queues.put(queue, Collections.emptyList());
		return ret;
	}

	@Override
	public void closeQueueAndReallocate(Q queue) {
		final List<T> ret = dequeueAllFromQueue(queue);
		
		for (Entry<Q, List<T>> entry: queues.entrySet()){
			if (! entry.getKey().equals(queue)) {
				queues.put(entry.getKey(), ret);
				break;
			}
		}		
		
		queues.remove(queue);
	}

}
