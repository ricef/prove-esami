package ex2016.a03a.e1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;
import java.util.function.ToIntFunction;
import java.util.function.UnaryOperator;

public class BagFactoryImpl implements BagFactory {

	@Override
	public <X> Bag<X> empty() {
		return new BagImpl<>(Collections.emptyList());
	}

	@Override
	public <X> Bag<X> fromSet(Set<X> set) {
		return new BagImpl<>(set);
	}

	@Override
	public <X> Bag<X> fromList(List<X> list) {
		// TODO Auto-generated method stub
		return new BagImpl<>(list);
	}

	@Override
	public <X> Bag<X> bySupplier(Supplier<X> supplier, int nElements, ToIntFunction<X> copies) {
		final List<X> maker = new ArrayList<>();
		for (int i=0;i<nElements; i++) {
			X element = supplier.get();
			for (int j=0;j<5;j++) {
				maker.add(element);
			}
		}
		return new BagImpl<>(maker);
	}

	@Override
	public <X> Bag<X> byIteration(X first, UnaryOperator<X> next, int nElements, ToIntFunction<X> copies) {
		
		return null;
	}

}
