package ex2016.a03a.e2;

public interface Model {
	int getA();
	int getB();
	
	void incrementA();
	void incrementB();
	
	boolean movesDone();
}
