package ex2016.a03a.e2;

public class ModelImpl implements Model {

	private final int size;
	private int indexA;
	private int indexB;
	
	public ModelImpl(final int size) {
		this.size = size;
		indexA=0;
		indexB=1;
	}
	
	@Override
	public int getA() {
		return indexA;
	}

	@Override
	public int getB() {
		return indexB;
	}

	@Override
	public void incrementA() {
		if ((indexA+1) < indexB) {
			indexA++; 
		}
	}

	@Override
	public void incrementB() {
		if ((indexB+1)<size) {
			indexB++;
		}
		
	}
	
	public boolean movesDone() {
		return (indexA == size-2);
	}

}
