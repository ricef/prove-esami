package ex2016.a03a.e2;

import javax.swing.*;
import java.util.*;

public class GUI {
    
	private final List<JButton> buttons;
	private Model model;
    
    public GUI(final int size){
    	final JPanel jp = new JPanel();
    	final JFrame jf = new JFrame();
    	final JButton moveA = new JButton("Move A");
    	final JButton moveB = new JButton("Move B");
    	final JButton reset = new JButton("Reset");
    	buttons = new ArrayList<>();
    	model = new ModelImpl(size);

    	for (int i=0;i<size;i++) {
    		buttons.add(new JButton(" "));
    		buttons.get(i).setEnabled(false);
    		jp.add(buttons.get(i));
    	}
    	
    	
    	moveA.addActionListener(e ->{
    		model.incrementA();
    		set();
    	});
    	
    	moveB.addActionListener(e ->{
    		model.incrementB();
    		set();
    		if (model.movesDone()) System.exit(1);
    	});
    	
        reset.addActionListener(e -> {
        	model = new ModelImpl(size);
        	set();
        	if (model.movesDone()) System.exit(1);
        });
        
        jp.add(moveA);
        jp.add(moveB);
        jp.add(reset);
        
        set();
        
        jf.getContentPane().add(jp);
        jf.pack();
        jf.setVisible(true);
    }
    
    private void set () {
    	buttons.forEach(el -> el.setText(" "));
    	buttons.get(model.getA()).setText("A");
    	buttons.get(model.getB()).setText("B");
    }
    
    public static void main(String[] s){
        new GUI(10);
    }

}
