package a02b.e1;

import java.util.ArrayList;
import java.util.List;

public class TransducerFactoryImpl implements TransducerFactory {

	
	
	@Override
	public <X> Transducer<X, String> makeConcatenator(int inputSize) {
		return new Transducer<X,String>(){

			private final List<String> output = new ArrayList<>();
			private final List<X> input = new ArrayList<>();
			private final int size = inputSize;
			private boolean isInputOver = false;
			
			@Override
			public void inputIsOver() {
				if (isInputOver == false) {
					isInputOver = true;
					if (! input.isEmpty()) {
						addToOutput(input.size());
					}
				} else {
					throw new IllegalStateException();
				}
			}
			
			@Override
			public boolean isNextOutputReady() {
				return ! output.isEmpty();
			}

			@Override
			public String getOutputElement() {
				if (output.size() != 0) {
					return output.remove(0);
				} else {
					throw new IllegalStateException();
				}
			}

			@Override
			public boolean isOutputOver() {
				return (output.isEmpty() && isInputOver);
			}

			@Override
			public void provideNextInput(X x) {
				if ( ! isInputOver ) {
					input.add(x);
				}
				if (input.size() == size) {
					addToOutput(size);
				}
			}
			
			protected void addToOutput(int size) {
				java.lang.String s  = "";
				for (int i=0; i<size; i++) {
					s = s + input.remove(0).toString();
				}
				output.add(s);
			}
			
			
			
		};
	}
	
	

	@Override
	public Transducer<Integer, Integer> makePairSummer() {
		// TODO Auto-generated method stub
		return new Transducer<Integer, Integer>() {
			
			private final List<Integer> output = new ArrayList<>();
			private final List<Integer> input = new ArrayList<>();
			private static final int size = 2;
			private boolean isInputOver = false;
			
			@Override
			public void inputIsOver() {
				if (isInputOver == false) {
					isInputOver = true;
					if (! input.isEmpty()) {
						addToOutput(input.size());
					}
				} else {
					throw new IllegalStateException();
				}
			}
			
			@Override
			public boolean isNextOutputReady() {
				return ! output.isEmpty();
			}

			@Override
			public Integer getOutputElement() {
				if (output.size() != 0) {
					return output.remove(0);
				} else {
					throw new IllegalStateException();
				}
			}

			@Override
			public boolean isOutputOver() {
				return (output.isEmpty() && isInputOver);
			}

			@Override
			public void provideNextInput(Integer x) {
				if ( ! isInputOver ) {
					input.add(x);
				}
				if (input.size() == size) {
					addToOutput(size);
				}
			}
			
			private void addToOutput(int size) {
				Integer e = 0;
				for (int i=0; i<size; i++) {
					e = e + input.remove(0);
				}
				output.add(e);
			}
		};
	}

}
