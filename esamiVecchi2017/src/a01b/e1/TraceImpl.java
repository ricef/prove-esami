package a01b.e1;


import java.util.Iterator;
import java.util.Optional;

public class TraceImpl<X> implements Trace<X> {

	private final Iterator<Event<X>> it ;
	
	public TraceImpl(final Iterator<Event<X>> from) {
		it = from;
	}
	
	@Override
	public Optional<Event<X>> nextEvent() {
		return (it.hasNext()? Optional.of(it.next()) : Optional.empty());
	}

	@Override
	public Iterator<Event<X>> iterator() {
		return it;
	}

	@Override
	public void skipAfter(int time) {
		while (it.hasNext() && it.next().getTime()<time) {}
	}

	@Override
	public Trace<X> combineWith(Trace<X> trace) {
		return new TraceImpl<>(new Iterator<Event<X>>() {
			private Optional<Event<X>> e1 = trace.nextEvent();
			private Optional<Event<X>> e2 = nextEvent();
 			
			@Override
			public boolean hasNext() {
				return e1.isPresent() || e2.isPresent();
			}

			@Override
			public Event<X> next() {
				final Event<X> ret;
				if (! e1.isPresent() || e2.isPresent() && e2.get().getTime() < e1.get().getTime()) {
					ret = e2.get();
					e2=nextEvent();
					
				} else {
					ret = e1.get();
					e1=trace.nextEvent();
				}
				return ret;
			}
		});
	}

	@Override
	public Trace<X> dropValues(X value) {
		// TODO Auto-generated method stub
		return null;
	}

}
