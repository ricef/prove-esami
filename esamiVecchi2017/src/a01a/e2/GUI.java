package a01a.e2;


import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;

public class GUI extends JFrame{

	private static final long serialVersionUID = 1L;
	private final List<JButton> buttons = new ArrayList<>();
	private Model model;
	
	public GUI(final int size){
		this.setSize(500, 100);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new FlowLayout());
		
		final JButton print = new JButton("Print");
		
		model = new ModelImpl(size);
		
		for ( int i = 0; i<size; i++) {
			final int index = i;
			buttons.add(new JButton ("0"));
			buttons.get(i).addActionListener(e ->{
				buttons.get(index).setText(model.getValueOf(index));
				if (! model.isEnabled(index)) {
					buttons.get(index).setEnabled(false);
				}
			});
			this.getContentPane().add(buttons.get(i));
		}
		
		print.addActionListener(e -> model.getValues().forEach(el -> System.out.println(el)));

		this.getContentPane().add(print);
		this.setVisible(true);
	}

}
