package a01a.e2;

import java.util.List;

public interface Model {
	
	String getValueOf(int index);
	
	List<Integer> getValues();
	
	boolean isEnabled (int index);

}
