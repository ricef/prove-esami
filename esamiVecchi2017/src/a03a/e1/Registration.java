package a03a.e1;

public class Registration {

	private final Exam exam;
	private final Student student;
	
	public Registration(final Exam exam, final Student student) {
		this.exam = exam;
		this.student = student;
	}
	
	public Exam getExam() {
		return exam;
	}
	
	public Student getStudent() {
		return student;
	}
}
