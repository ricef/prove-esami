package a03a.e1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

public class ExamsManagementImpl implements ExamsManagement{

	private final List<Student> students;
	private final List<Exam> exams;
	private Optional<Exam> currentExam;
	
	private final List<Registration> registered;
	private final Map <Registration, Optional<Integer>> voti;
	
	public ExamsManagementImpl() {
		students = new ArrayList<>();
		exams = new ArrayList<>();
		registered = new ArrayList<>();
		voti = new HashMap<>();
		currentExam = Optional.empty();
	}
	
	@Override
	public void createStudent(int studentId, String name) {
		students.add(new Student(studentId, name));
	}

	@Override
	public void createExam(String examName, int incrementalId) {
		exams.add(new Exam(incrementalId, examName));		
	}

	@Override
	public void registerStudent(String examName, int studentId) {
		Optional<Student> j = Optional.empty();
		for (Student s : students) {
			if( s.getId() == studentId) {
				j = Optional.of(s);
			}
		}
		
		Optional<Exam> i = Optional.empty();
		for (Exam e : exams) {
			if (e.getName().equals(examName)) {
				i = Optional.of(e);
			}
		}
		
		if (i.isPresent() && j.isPresent()) {
			registered.add(new Registration(i.get(), j.get()));
			System.out.println("Added to " + i.get().getName() + " " + j.get().getName());
		}
	}

	@Override
	public void examStarted(String examName) {
		if( !examName.equals(exams.get(indexOfNextExam()).getName()) || currentExam.isPresent() ) {
			throw new IllegalStateException();
		} else {
			currentExam = Optional.of(exams.remove(indexOfNextExam()));
			for (Registration r : registered) {
				if (currentExam.get().getName().equals(r.getExam().getName())){
					voti.put(new Registration(currentExam.get(), r.getStudent()), Optional.empty());
				}
			}
		}
		
	}

	@Override
	public void registerEvaluation(int studentId, int evaluation) {
		if (currentExam.isPresent()) {
			voti.entrySet().forEach(entry ->{
				if (entry.getKey().getStudent().getId() == studentId && 
						entry.getKey().getExam().getName().equals(currentExam.get().getName())) {
					voti.put(entry.getKey(), Optional.of(evaluation));
				}
			});
		} else {
			throw new IllegalStateException();
		}
		
	}

	@Override
	public void examFinished() {
		
		if (currentExam.isPresent()) {
			currentExam = Optional.empty();
		} else {
			throw new IllegalStateException();
		}
	}

	@Override
	public Set<Integer> examList(String examName) {
		final Set<Integer> set = new HashSet<>();
		voti.entrySet().forEach(entry -> {
			if (entry.getKey().getExam().getName().equals(examName)) {
				set.add(entry.getKey().getStudent().getId());
			}
		});
		return set;
	}

	@Override
	public Optional<Integer> lastEvaluation(int studentId) {
		Optional<Integer> vote = Optional.empty();
		int examId = 0;
		
		for(Entry<Registration,Optional<Integer>> entry : voti.entrySet()) {
			if (entry.getKey().getStudent().getId() == studentId) {
				if (entry.getKey().getExam().getId() > examId) {
					examId = entry.getKey().getExam().getId();
					vote = entry.getValue();
				}
			}
		}
		
		return vote;
	}

	@Override
	public Map<String, Integer> examStudentToEvaluation(String examName) {
		final Map<String, Integer> ret = new HashMap<>();
		for(Entry<Registration,Optional<Integer>> entry : voti.entrySet()) {
			if (entry.getKey().getExam().getName().equals(examName) && entry.getValue().isPresent()) {
				System.out.println(examName + " / " + entry.getKey().getExam().getName());
				System.out.println(entry.getKey().getStudent().getName() + " " + entry.getValue().get());
				ret.put(entry.getKey().getStudent().getName(), entry.getValue().get());
			}
		}
		return ret;
	}

	@Override
	public Map<Integer, Integer> examEvaluationToCount(String examName) {
		// TODO Auto-generated method stub
		return null;
	}
	
	private int  indexOfNextExam() {
		Exam ret = exams.get(0);
		for ( Exam e: exams) {
			if(e.getId() < ret.getId()) {
				ret = e;
			}
		}
		
		return exams.indexOf(ret);
	}

}
