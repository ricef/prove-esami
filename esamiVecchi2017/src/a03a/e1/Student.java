package a03a.e1;

public class Student {
	private final int id;
	private final String name;
	
	public Student(final int id, final String name) {
		this.id = id;
		this.name = name;
	}
	
	public final int getId() {
		return this.id;
	}
	
	public final String getName() {
		return this.name;
	}
}
