package a03a.e1;

public class Exam {
	private final int id;
	private final String name;
	
	public Exam(final int id, final String name) {
		this.id = id;
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getId() {
		return this.id;
	}

}
