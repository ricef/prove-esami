package a03a.e2;

public interface Model {

	int getLinesNumber();
	String getLineValue(int index);
	void disable (int index);
	void append();
}
