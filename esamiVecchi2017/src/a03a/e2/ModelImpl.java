package a03a.e2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ModelImpl implements Model {

	private final List<Optional<String>> lines;
	private final List<Optional<String>> disabled;
	private final File file;
	
	public ModelImpl(final String file) {
		lines = new ArrayList<>();
		this.file = new File(file);
		
		try ( final BufferedReader reader = new BufferedReader(
				new InputStreamReader( new FileInputStream(file)))){
			reader.lines().forEach(line ->{
				lines.add(Optional.of(line));
			});
		} catch (FileNotFoundException e) {
			System.out.println("FILE NOT FOUND");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("IO EXCEPTION");
			e.printStackTrace();
		}
		
		disabled = new ArrayList<>(lines);
	}
	
	@Override
	public int getLinesNumber() {
		return lines.size();
	}
	@Override
	public String getLineValue(int index) {
		return lines.get(index).get();
	}
	@Override
	public void disable(int index) {
		disabled.set(index, Optional.empty());
	}

	@Override
	public void append() {
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))){
			for (Optional<String> line : lines){
				if (line.isPresent()) {
					writer.write(line.get());
					writer.newLine();
				}
			}
			for (Optional<String> line : disabled) {
				if (line.isPresent()) {
					writer.write(line.get());
					writer.newLine();
				}
			}
		}catch (IOException e) { 
			System.out.println("IO EXCEPTION");
			e.printStackTrace();
		}
	}

}
