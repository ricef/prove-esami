package a02a.e1;

public class Power2Sequence extends BasicSequence{

	@Override
	public void reset() {
		actual = 1;
		next = 2;
	}

	@Override
	public void acceptElement(int i) {
		if (i == actual) {
			final int temp = next;
			next = next*next;
			actual = temp;
		} else {
			throw new IllegalStateException();
		}
	}

}
