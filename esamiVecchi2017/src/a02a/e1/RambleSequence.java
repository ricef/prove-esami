package a02a.e1;

public class RambleSequence extends BasicSequence{

	@Override
	public void reset() {
		actual = 0 ;
		next = 1;
	}

	@Override
	public void acceptElement(int i) {
		if (actual == i) {
			final int temp = next+1;
			next = actual;
			actual =temp;
		} else {
			throw new IllegalStateException();
		}
	}

}
