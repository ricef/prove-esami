package a02a.e2;

public class ModelImpl implements Model {

	private final boolean[][] mat;
	
	public ModelImpl(int size) {
		System.out.println("Model creato.");
		mat = new boolean[size][size];
		System.out.println(mat.toString());
	}
	
	@Override
	public boolean hit(final int i, final int j) {
		System.out.println(mat[i][j]);
		if (mat[i][j] == true) {
			mat[i][j] = false;
		} else {
			mat[i][j] = true;
		}
		return mat[i][j];
	}

	@Override
	public boolean isFull(final int i, final int j) {
		
		if (isRowFull(i) || isColumnFull(j)) {
			return true;
		}
		return false;
	}
	
	private boolean isColumnFull(final int j) {
		for (int index=0;index<mat.length; index++) {
			if (mat[index][j] == false) {
				return false;
			}
		}
		return true;
	}
	
	private boolean isRowFull(final int i) {
		for (boolean vec : mat[i]) {
			if (vec == false) {
				return false;
			}
		}
		return true;
	}

}
