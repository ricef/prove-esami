package a02a.e2;

import javax.swing.*;
import java.util.*;
import java.util.List;
import java.awt.*;

public class GUI extends JFrame {
    
	private static final long serialVersionUID = 70923817406855375L;
	private final List<List<JButton>> buttons = new ArrayList<>();
	private final Model model;
	
	public GUI(int size) {
         final JPanel panel = new JPanel(new GridLayout(size,size));
         
         model = new ModelImpl(size);
         
         for( int i= 0; i<size;i++) {
        	 buttons.add(new ArrayList<JButton>());
        	 for (int j=0; j<size; j++) {
        		 final int indexi = i;
        		 final int indexj = j;
        		 buttons.get(i).add(new JButton(" "));
        		 buttons.get(i).get(j).addActionListener(e->{
        			 if (model.hit(indexi,indexj)) {
        				 buttons.get(indexi).get(indexj).setText("*");
        			 } else {
        				 buttons.get(indexi).get(indexj).setText(" ");
        			 }
        			 if (model.isFull(indexi,indexj)) {
        				 System.out.println("E' pieno");
        				 System.exit(1);
        			 }
        			 System.out.println("Non e' pieno");
        		 });
        		 
        		 panel.add(buttons.get(i).get(j));
        	 }
         }
         
         this.setDefaultCloseOperation(EXIT_ON_CLOSE);
         this.setSize(500, 500);
         this.getContentPane().add(BorderLayout.CENTER,panel);
         this.setVisible(true);
    }
    
    
}
